/// <reference types="cypress" />


it('Equaliy', () => {
    const a = 1;

    expect(a).equal(1);
    expect(a, 'deveria ser 1').equal(1);
    expect(a).to.be.equal(1);
    expect('a').not.to.be.equal('b')
})


it('Truthy', () => {
  const a = true;
  const b = null;
  let c;


    expect(a).to.be.true;
    expect(true).to.be.true;
    expect(b).to.be.null;
    expect(a).not.to.be.null;
    expect(a).to.be.not.null;
    expect(c).to.be.undefined;

})


it('Object equality', () => {
    const obj = {
        a:1,
        b:2
    }

    expect(obj).equal(obj);
    expect(obj).equals(obj);
    expect(obj).eq(obj)
    expect(obj).to.be.equal(obj)

    expect(obj).to.be.deep.equal({a:1, b:2});
    expect(obj).eql({a:1, b:2});
    expect(obj).include({a: 1})
    expect(obj).include({b: 2})
    expect(obj).to.have.property('b');
    expect(obj).to.have.property('b', 2);
    expect(obj).to.not.be.empty
    expect({}).to.be.empty
})


it('Arrays', () => {
    const Arrays = [1,2,3]
    expect(Arrays).to.have.members([1, 2, 3])


})

it('Arrays2', () => {
    const Arr = [1,2,3]
    expect(Arr).to.have.members([1, 2, 3])
    expect(Arr).to.include.members([1,3]);
    expect(Arr).to.not.empty
    expect([]).to.be.empty

})

it('types', () => {
    const num = 1
    const str = 'String'

    expect(num).to.be.a('number')
    expect(str).to.be.a('string');
    expect({}).to.be.a.an('object')
    expect([]).to.be.a.an('array')
})

it('String', () => {
    const str = 'String de teste'

    expect(str).to.be.equal('String de teste');
    expect(str).to.have.length(15);
    expect(str).to.contains('de')
    expect(str).to.match(/de/)
    expect(str).to.match(/^String/)    //iniciar ^
    expect(str).to.match(/teste$/)     //finalizar com ... $
    expect(str).to.match(/.{15}/)      //tamanho
    expect(str).to.match(/\W+/)       // verifica se existe apenas letras
    expect(str).to.match(/\D+/)       // verifica se nao existe numeros  

})


it('Numbers', () => {
    const number = 4
    const floatNumber = 5.2123

    expect(number).to.be.equal(4)
    expect(number).to.be.above(3)
    expect(number).to.be.below(7)
    expect(floatNumber).to.be.equal(5.2123)
    expect(floatNumber).to.be.closeTo(5.2, 0.1)
    expect(floatNumber).to.be.above(5)

})