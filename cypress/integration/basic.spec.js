/// <reference types="cypress" />

describe('cypress basics', ()  => {
    it('Should visit a page and assert title', () => {
        cy.visit('https://wcaquino.me/cypress/componentes.html')

        // const title = cy.title()
        // console.log(title)

        cy.title().should('be.equal', 'Campo de Treinamento')
        cy.title().should('contain', 'Campo')
    

        cy.title()
            .should('be.equal', 'Campo de Treinamento')
            .and('contain', 'Campo')
    
        })
        
    // it('cypress record test', () => {
    //     cy.visit('https://wcaquino.me/cypress/componentes.html');
    //     cy.get('#formNome').click();
    //     cy.get('#formNome').type('ola');
    //     cy.get('[data-cy=dataSobrenome]').type('ola');
    //     cy.get('#formSexoMasc').click();
    //     cy.get('#formComidaCarne').click();

    // })

        it('should find and interact with an element', () => {
            cy.visit('https://wcaquino.me/cypress/componentes.html')
            cy.get('#buttonSimple')
                .click()
                .should('have.value', 'Obrigado!')
            cy.get('#buttonLazy')
                .click()
                .wait(4000)
                .should('have.value', 'zZz ZzZ!')
                cy.get('[name="elementosForm:sugestoes"]')
                .type('Ola')

            

        })
        


})